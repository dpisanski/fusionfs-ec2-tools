#!/usr/bin/env bash

_hostname="$(hostname -I)"
hostname="$(echo -e "${_hostname}" | tr -d '[[:space:]]')"

for (( i = 1; i < 2; i++ )); do
    grep -l $1 /mnt/fusion_mount/${hostname}.${i}
done
