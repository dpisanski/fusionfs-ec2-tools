#!/usr/bin/env bash

# clean_data.sh: Cleans up the files made during benchmarking. Removes all files 
# in the fusion mountpoint, index, and random data generated by `gen_data.sh`.

source config

main() {
    # Note: tries deleting $FUSION_INDEX, which doesn't exist on the non-indexing
    # code. rm will complain, but nothing crashes
    parallel-ssh -vi -l ${EC2_USER} -x "-i ${EC2_KEY}" -h ${PUBLIC_IPS} \
        "rm -rf ${FUSION_MOUNT}/*"
}

main
