# Instructions

The ec2 tools require the user to create two files, `hosts` and `neighbor.conf`.

## neighbor.conf

Store the private IP of each node, along with the port fusionfs listens to
on each line like so:
    private-ip port-num

## hosts

Store the public IP of each node line by line. The parallel-* tools use this,
along with your pemkey to connect to the nodes.
