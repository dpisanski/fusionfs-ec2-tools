#!/usr/bin/env bash

source config

# $1: The query string
main() {
    local -r query=$1
    local -r search_out=.search

    if [[ -d  ${search_out} ]]; then
        rm ${search_out}/*
    fi

    time parallel-ssh -l ${EC2_USER} -x "-i ${EC2_KEY}" -h ${PUBLIC_IPS} \
        -o ${search_out} "ffsearch ${FUSION_INDEX} ${query}" 

    # parallel-ssh creates the output directory, so we can read the contents.
    cat ${search_out}/*

}

main $1 
