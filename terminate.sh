#!/usr/bin/env bash

source config

main() {
    parallel-ssh -vi -l ${EC2_USER} -x "-i ${EC2_KEY}" -h ${PUBLIC_IPS} \
            "${FUSION_BUILD_ROOT}/script/stop.sh 2>&1 >/dev/null &"
                                                                 
    parallel-ssh -vi -l ${EC2_USER} -x "-i ${EC2_KEY}" -h ${PUBLIC_IPS} \
            "${FUSION_BUILD_ROOT}/script/stop_service.sh 2>&1 >/dev/null &"
}

main
