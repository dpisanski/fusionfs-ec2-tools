#!/usr/bin/env python
# encoding: utf-8

import sys
import socket
import string
import commands
import random
import os

FILENAME_LENGTH = 15
DIR_SIZE_LIMIT = 50 
WORD_LIST = "american-english"

def random_string(length):
    pool = string.letters + string.digits
    return ''.join(random.choice(pool) for i in range(length))

def random_word(words, maxjoin=1):
    """Generate a random word from a given list of words.

    :words: A list of possible words. 
    :maxjoin: How many possible words can be joined together to form a new word?
    """
    word = ""
    for _ in range(random.randint(1, maxjoin)):
        word += words[random.randint(0, len(words) -1)]

    return word


def random_sentence(words, wordcount=10):
    """Generates a string of random words with spaces in between.

    :words: The list of possible words.
    :wordcount: Number of words in each sentence.
    """
    return " ".join([random_word(words) for _ in range(wordcount)])

def generate_file(path, words, size):
    """Fill a file with a random selection of english words.
    Fills the file until it has reached the given size.
    Note: will create the file if it does not exist yet. However, it will not
    create any missing directories.

    :path: Full path to the file.
    :words: List of English words to choose from.
    :size: The total number of bytes to be written to the file.
    """
    with open(path, "w") as outfile:
        while outfile.tell() < size:
            outfile.write(random_sentence(words))
            outfile.write("\n")

def generate_dataset_old(dir, words, filesize, filecount):
    """Generate the dataset and store it in dir. 
    FusionFS has a limit on the number of items in a directory. Nested
    directories allow us to store many files within FusionFS.
    """
    hostname = commands.getoutput("hostname -I").strip()
    files_made = 1
    for _ in range(DIR_SIZE_LIMIT):
        first = random_string(FILENAME_LENGTH)
        for _ in range(DIR_SIZE_LIMIT):
            second = random_string(FILENAME_LENGTH)
            # Make 
            fpath = os.path.join(dir, first, second)
            os.makedirs(fpath)
            for _ in range(DIR_SIZE_LIMIT):
                fname = hostname + "." + str(files_made)
                generate_file(os.path.join(fpath, fname), words, filesize)
                files_made += 1

                if files_made > filecount:
                    return

def generate_dataset_old(dir, words, filesize, filecount):
    hostname = commands.getoutput("hostname -I").strip()
    files_made = 1
    for _ in range(DIR_SIZE_LIMIT):
        fname = ".".join([hostname, str(files_made)])
        fpath = os.path.join(dir, fname)
        generate_file(fpath, words, filesize)
        files_made += 1
        if files_made > filecount:
            return


def main(argv):
    if len(argv) != 4:
        print "Usage: %s output-directory filesize filecount" % (argv[0])
        sys.exit(2)

    random.seed(os.urandom(100))

    data_dir = argv[1]
    filesize = int(argv[2])
    filecount = int(argv[3])
    hostname = commands.getoutput("hostname -I").strip()
    # hostname = random_string(FILENAME_LENGTH)

    with open(WORD_LIST) as word_list:
        words = [line.strip("\n") for line in word_list]

    # output_dir = os.path.join(data_dir, hostname)
    output_dir = os.path.join(data_dir)
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    generate_dataset(output_dir, words, filesize, filecount)

#    for _ in range(filecount):
#        random_filename = random_string(FILENAME_LENGTH)
#        generate_file(os.path.join(output_dir, random_filename), words, filesize)
#

    #        second = random_string(FILENAME_LENGTH)
    #        for _ in range(DIR_SIZE_LIMIT):
    #            third = random_string(FILENAME_LENGTH)
    #            root = os.path.join(data_dir, hostname, first, second, third)
    #            try:
    #                os.makedirs(root)
    #            except:
    #                print "Error making directory=", root
    #                continue
    #            for _ in range(DIR_SIZE_LIMIT):
    #                pass
    #            
    #for root, dirs, files in os.walk(os.path.join(data_dir, hostname), topdown=false):
    #    print "root=", root, " dirs=", dirs, " files=", files

if __name__ == '__main__':
    main(sys.argv)
