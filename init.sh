#!/usr/bin/env bash

# init.sh: Retrieves the private and public IP adresses of your running ec2 instances, 
# then connects to each node and launches fusionfs. 

source config

# get_ips: fetches the public and private ips of all running ec2 instances
get_ips() {
    aws ec2 describe-instances --filter "Name=instance-state-name,Values=running" \
        --query "Reservations[*].Instances[*].PublicIpAddress[]" \
        --output=text | tr '\t' '\n' > ${PUBLIC_IPS}

    aws ec2 describe-instances --filter "Name=instance-state-name,Values=running" \
        --query "Reservations[*].Instances[*].PrivateIpAddress[]" \
        --output=text | tr '\t' '\n' > ${PRIVATE_IPS}

    sed "s/$/ ${FUSION_PORT_NUM}/" ${PRIVATE_IPS} > ${NEIGHBORCONF}
}

# load_files: load various files to each of the nodes. 
load_files() {
    parallel-scp -v -l ${EC2_USER} -x "-i ${EC2_KEY}" -h ${PUBLIC_IPS} \
        neighbor.conf ${FUSION_BUILD_ROOT}/conf

    parallel-scp -v -l ${EC2_USER} -x "-i ${EC2_KEY}" -h ${PUBLIC_IPS}  \
        data_gen.py ${FUSION_MOUNT_ROOT}

    parallel-scp -v -l ${EC2_USER} -x "-i ${EC2_KEY}" -h ${PUBLIC_IPS}  \
        american-english ${FUSION_MOUNT_ROOT}

    parallel-scp -v -l ${EC2_USER} -x "-i ${EC2_KEY}" -h ${PUBLIC_IPS}  \
        node-grep.sh ${FUSION_MOUNT_ROOT}
}

# start_fusionfs: launches the start scripts on each of the nodes.
# Note: start_service.sh must successfully complete on each node before calling start.sh.
# TODO: Don't hardcode the location of the scripts
start_fusionfs() {
    parallel-ssh -vi -l ${EC2_USER} -x "-i ${EC2_KEY}" -h ${PUBLIC_IPS} \
        "${FUSION_BUILD_ROOT}/script/start_service.sh 2>&1 >/dev/null &"

    parallel-ssh -vi -l ${EC2_USER} -x "-i ${EC2_KEY}" -h ${PUBLIC_IPS} \
        "${FUSION_BUILD_ROOT}/script/start.sh 2>&1 >/dev/null &"
}

main() {
    echo "getting ips"
    get_ips
    echo "loading files"
    load_files
    echo "starting fusionfs"
    start_fusionfs
}

main
