#!/usr/bin/env bash

source config

# benchmark.sh: copy files from $DATADIR into $FUSION_MOUNT and time it
# TODO work on indexing + non indexing versions

main() {
    time parallel-ssh -t 0 -l ${EC2_USER} -x "-i ${EC2_KEY}" -h ${PUBLIC_IPS} \
        "cp -r -t ${FUSION_MOUNT} ${DATADIR}/*"
}

main
