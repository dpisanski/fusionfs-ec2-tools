#!/usr/bin/env bash

# gen_data.sh: Generate testing data locally on each node, and store it in $OUTDIR. 
# Runs `data_gen.py` on each node. 

source config

# $1: filesize
# $2: filecount
main() {    
    local -r filesize=$1
    local -r filecount=$2

    parallel-ssh -iv -t 0 -l ${EC2_USER} -x "-i ${EC2_KEY}" -h ${PUBLIC_IPS} \
        "python ${FUSION_MOUNT_ROOT}/data_gen.py ${DATADIR} $filesize $filecount"

}

main $1 $2
