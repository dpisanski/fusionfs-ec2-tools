#!/usr/bin/env bash

source config

# $1: string to search for
main() {
    time parallel-ssh -vi -l ${EC2_USER} -x "-i ${EC2_KEY}" -h ${PUBLIC_IPS} \
        "bash ${FUSION_MOUNT_ROOT}/node-grep.sh $1"
}

main $1
